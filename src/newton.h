/** 
 * physicalib is a C library for physics computation that can be used in 
 * applications, software, mobile applications, server and basically everywhere.
 * It is related mainly with science, it also may be used if you don't want to convert
 * Newton's law (for example) to a C function! In that case you can just use the library implementation.
 * There also some already mini-functions for mathematics that I created just to make 
 * the life of a C programmer a bit of easier.
 * ---
 * Created by Edward Pasenidis / code available on GitLab
 * ---
 * Main file: main.h
*/

#include "deps.h"

float l_masstacc_i(float mass)
{
    char pl[MAX_LIMIT];
    fgets(pl, MAX_LIMIT, stdin);
    if ( 0 == strcmp(pl, "1") )
    {
        // approximate calculation for Earth
        return mass * 9.81;
    }
    else if ( 0 == strcmp(pl, "2") )
    {
        // approximate calculation for Mars
        return mass * 3.71;
    }
    else if ( 0 == strcmp(pl, "3") )
    {
        // approximate calculation for Moon (Earth's natural satellite)
        return mass * 1.62;
    }
    else
    {
        return mass * 9.81;
    }
}

float l_masstacc_io(float mass)
{
    char pl[MAX_LIMIT];
    fgets(pl, MAX_LIMIT, stdin);
    if ( 0 == strcmp(pl, "1") )
    {
        // approximate calculation for Earth
        printf("liboutput: %f\n", mass * 9.81);
    }
    else if ( 0 == strcmp(pl, "2") )
    {
        // approximate calculation for Mars
        printf("liboutput: %f\n", mass * 3.71);
    }
    else if ( 0 == strcmp(pl, "3") )
    {
        // approximate calculation for Moon (Earth's natural satellite)
        printf("liboutput: %f\n", mass * 1.62);
    }
    else
    {
        printf("liboutput: (default parameters) %f\n", mass * 9.81);
    }
}


/**
 * This function computes the 3rd Law of Newton (Action / Reaction)
 * Fab = -Fba
 * @param force the force value
 * @return the force
*/

float l_newtonthree(float force)
{
    // Turn the positive number into a negative number
    return force * -1;
}

float l_newtonthree_o(float force)
{
    printf("liboutput: %f\n", (force * (float) -1));
}

float l_unigravity(double mass1, double mass2, double distance)
{
    float F = (BIG_G * mass1 * mass2) / pow(distance, 2);
    return F;
}

float l_unigravity_i(void)
{
    float distance, a, b;
    printf("\n");
    printf("libinput: => A =  ");
    scanf("%f", &a);
    printf("libinput: => B =  ");
    scanf("%f", &b);
    printf("libinput: => radius = ");
    scanf("%f", &distance);
    float F = (BIG_G * a * b) / pow(distance, 2);
    return F;
}

float l_unigravity_io(void)
{
    float distance, a, b;
    printf("\n");
    printf("libinput: => A =  ");
    scanf("%f", &a);
    printf("libinput: => B =  ");
    scanf("%f", &b);
    printf("libinput: => radius = ");
    scanf("%f", &distance);
    float F = (BIG_G * a * b) / pow(distance, 2);
    printf("liboutput: => F = %f\n", F);
}