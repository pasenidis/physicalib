/** 
 * physicalib is a C library for physics computation that can be used in 
 * applications, software, mobile applications, server and basically everywhere.
 * It is related mainly with science, it also may be used if you don't want to convert
 * Newton's law (for example) to a C function! In that case you can just use the library implementation.
 * There also some already mini-functions for mathematics that I created just to make 
 * the life of a C programmer a bit of easier.
 * ---
 * Created by Edward Pasenidis / code available on GitLab
 * ---
 * Main file: main.h (current)
*/

#ifndef PHYSICA_H
#define PHYSICA_H

#include "deps.h"
#include "hydropress.h"
#include "newton.h"

/**
 * Return an addition
 * @param a is the first operand
 * @param b is the second operand
 * @return first operand + second operand
*/

double add(int a, int b)
{
    return a + b;
}

/**
 * Prompts user for input two times (logged), then returns a + b
*/

double add_i(void)
{
    int a, b;
    printf("A: ");
    scanf("%i", &a);
    printf("B: ");
    scanf("%i", &b);
    return add(a, b);
}

/**
    Prompts user for input two times, then prints the a + b
*/

double add_io(void)
{
    int a, b;
    printf("A: ");
    scanf("%i", &a);
    printf("B: ");
    scanf("%i", &b);
    printf("liboutput: %lf\n", add(a, b));
}

#endif