/** 
 * physicalib is a C library for physics computation that can be used in 
 * applications, software, mobile applications, server and basically everywhere.
 * It is related mainly with science, it also may be used if you don't want to convert
 * Newton's law (for example) to a C function! In that case you can just use the library implementation.
 * There also some already mini-functions for mathematics that I created just to make 
 * the life of a C programmer a bit of easier.
 * ---
 * Created by Edward Pasenidis / code available on GitLab
 * ---
 * Main file: main.h
*/

#include "deps.h"

float l_hydropress(float density, float gravity, float height)
{
    return density * gravity * height;
}

float l_hydropress_i(void)
{
    float a, b;
    char pl[MAX_LIMIT];
    printf("\n");
    printf("libinput: => density =  \n");
    scanf("%f", &a);
    printf("libinput: => height =  \n");
    scanf("%f", &b);
    printf("libinput: => code = \n");
    fgets(pl, MAX_LIMIT, stdin);
    if ( 0 == strcmp(pl, "1") )
    {
        // approximate calculation for Earth
        return l_hydropress(a, 9.81, b);
    }
    else if ( 0 == strcmp(pl, "2") )
    {
        // approximate calculation for Mars
        return l_hydropress(a, 3.71, b);
    }
    else if ( 0 == strcmp(pl, "3") )
    {
        // approximate calculation for Moon (Earth's natural satellite)
        return l_hydropress(a, 1.62, b);
    }
    else
    {
        return l_hydropress(a, 9.81, b);
    }
}

float l_hydropress_io(void)
{
    float a, b;
    char pl[MAX_LIMIT];
    printf("\n");
    printf("libinput: => density =  \n");
    scanf("%f", &a);
    printf("libinput: => height =  \n");
    scanf("%f", &b);
    printf("libinput: => code = \n");
    fgets(pl, MAX_LIMIT, stdin);
    if ( 0 == strcmp(pl, "1") )
    {
        // approximate calculation for Earth
        printf("liboutput: %f\n", l_hydropress(a, 9.81, b));
    }
    else if ( 0 == strcmp(pl, "2") )
    {
        // approximate calculation for Mars
        printf("liboutput: %f\n", l_hydropress(a, 3.71, b));
    }
    else if ( 0 == strcmp(pl, "3") )
    {
        // approximate calculation for Moon (Earth's natural satellite)
        printf("liboutput: %f\n", l_hydropress(a, 1.62, b));
    }
    else
    {
        printf("liboutput: %f\n", l_hydropress(a, 9.81, b));
    }
}