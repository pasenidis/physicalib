/** 
 * physicalib is a C library for physics computation that can be used in 
 * applications, software, mobile applications, server and basically everywhere.
 * It is related mainly with science, it also may be used if you don't want to convert
 * Newton's law (for example) to a C function! In that case you can just use the library implementation.
 * There also some already mini-functions for mathematics that I created just to make 
 * the life of a C programmer a bit of easier.
 * ---
 * Created by Edward Pasenidis / code available on GitLab
 * ---
 * Main file: main.h
*/

/**
 * These are some libraries physicalib does use for easier computation.
*/

#include <stdio.h>
#include <string.h>
#include <float.h>
#include <math.h>
#include <limits.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stddef.h>

/**
 * These are constants that physicalib depends on.
*/

#ifndef _DEPS_H_
#define _DEPS_H_
    #define MAX_LIMIT 2
    #define BIG_LIMIT 20

    double BIG_G = 6.6726 * pow(10, -11);
#endif