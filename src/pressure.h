/** 
 * physicalib is a C library for physics computation that can be used in 
 * applications, software, mobile applications, server and basically everywhere.
 * It is related mainly with science, it also may be used if you don't want to convert
 * Newton's law (for example) to a C function! In that case you can just use the library implementation.
 * There also some already mini-functions for mathematics that I created just to make 
 * the life of a C programmer a bit of easier.
 * ---
 * Created by Edward Pasenidis / code available on GitLab
 * ---
 * Main file: main.h
*/

#include "deps.h"

float l_solidpress(float verticalforce, float surfacearea)
{
    return verticalforce / surfacearea;
}

float l_solidpress_i(void)
{
    float verticalforce, surfacearea;
    printf("\n");
    printf("libinput: => Vertical Force =  ");
    scanf("%f", &verticalforce);
    printf("libinput: => Surface Area   =  ");
    scanf("%f", &surfacearea);
    return l_solidpress(verticalforce, surfacearea);
}

float l_solidpress_io(void)
{
    float verticalforce, surfacearea;
    printf("\n");
    printf("libinput: => Vertical Force =  ");
    scanf("%f", &verticalforce);
    printf("libinput: => Surface Area   =  ");
    scanf("%f", &surfacearea);
    printf("liboutput: %f\n", l_solidpress(verticalforce, surfacearea));
}